from flask import render_template, Flask
import pandas as pd

app = Flask(__name__)

@app.route("/")
def demo():

    kwargs = {
        'date_min' : (pd.Timestamp.now() - pd.Timedelta(days=365)).strftime("%Y-%m-%d"),
        'date_max' : pd.Timestamp.now().strftime("%Y-%m-%d"),
    }
    return render_template('flask/demo.html', **kwargs)